#!/bin/bash -x


unset GCE_COUNT
unset REGION_OVERRIDE
unset STARTING_INDEX

while getopts 'c:s:r:': option
do
    case "${option}" in
        c) GCE_COUNT=${OPTARG};;
        s) STARTING_INDEX="${OPTARG}";;
        r) REGION_OVERRIDE="${OPTARG}";;
    esac
done

usage()
{
  echo "Usage: ./install.sh   -c 1
                            [ -r us-west1 ]
                            [ -s 1 ]"
  exit 2
}

if [[ -z "${GCE_COUNT}" || ${GCE_COUNT} -le 0 ]]; then
    echo "Missing count variable"
    usage
    exit 0
fi

source ./variables.sh

# TODO: Download bmctl instead of using local logged in person's bmctl
# Make sure bmctl is installed
bmctl_installed

# Copy bmctl binary to bucket
BMCTL_LOC=$(which bmctl)
gsutil -q stat gs://${BUCKET_NAME}/utils/bmctl 2> /dev/null
if [[ $? > 0 ]]; then
  # Only create if doesn't exist
  gsutil cp ${BMCTL_LOC} gs://${BUCKET_NAME}/utils/bmctl
fi

##################################################
##################################################

# Setup boot disk
function setup_book_disk() {
    disk_name="ubuntu2004disk"
    # See if exists or not
    exists=$(gcloud compute disks list --filter=name=${disk_name} --format="value(name)")
    if [[ -z "${exists}" ]]; then
        gcloud compute disks create ${disk_name} \
            --image-project ubuntu-os-cloud --image-family ubuntu-2004-lts \
            --zone ${ZONE}

        gcloud compute images create ubuntu-2004-nested \
            --source-disk ubuntu2004disk \
            --source-disk-zone ${ZONE} \
            --licenses "https://www.googleapis.com/compute/v1/projects/vm-options/global/licenses/enable-vmx"
    fi
}

function create_gce_vms() {
    i=${CLUSTER_START_INDEX}
    end=$((GCE_COUNT + CLUSTER_START_INDEX))
    while [[ $i -lt ${end} ]]; do

      gcloud compute instances create abm-${i} \
        --description "GCE hosting KVM for Anthos Bare Metal ${i}" \
        --image ubuntu-2004-nested \
        --zone ${ZONE} \
        --boot-disk-size ${GCE_STORAGE_SIZE} \
        --boot-disk-type pd-ssd \
        --can-ip-forward \
        --network default \
        --labels type=edge-server \
        --tags http-server,https-server,kubectl \
        --scopes=https://www.googleapis.com/auth/cloud-platform \
        --metadata "startup-script-url=gs://${BUCKET_NAME}/gce-init.sh" \
        --machine-type ${MACHINE_SIZE} # --preemptible \

    let i=i+1
    done
}

function store_service_account_key() {
  KEY=$1
  GSA=$2
  PROJECT=${3:-$PROJECT_ID}

  if [[ -z "${KEY}" ]]; then
    2&> echo "Storing service account key as secret, but did not supply a key name"
    exit 1
  fi
  if [[ -z "${GSA}" ]]; then
    2&> echo "Storing service account key as secret, but did not supply a GSA name"
    exit 1
  fi

  gcloud secrets -q versions access latest --secret="${KEY}" 2> /dev/null
  if [[ $? > 0 ]]; then
    # only create a new key if one hasn't been created and stored in a secret
    gcloud iam service-accounts keys create ./key.json \
      --iam-account ${GSA}@${PROJECT}.iam.gserviceaccount.com \
      --project=${PROJECT}

    create_secret "${KEY}" "key.json" "true"

    rm -rf ./key.json
  fi
}


############################################################
###############  Main ######################################
############################################################

# Create or enable 4 required/recommended service accounts for project
# TODO: Upgrade to 3 different agents for each of the roles instead of the primary covering all
# create_enable_service_account "${GSA_CONNECT_AGENT}" "${PROJECT_ID}" "GKE Connect Agent Account"
# create_enable_service_account "${GSA_GKE_HUB}" "${PROJECT_ID}" "GKE Hub Service Account"
# create_enable_service_account "${GSA_OBSERVABILITY}" "${PROJECT_ID}" "GKE Observability Account"

# Generate a JSON key for GSA and store in a secret
store_service_account_key "${GSA_PRIMARY_SECRET_KEY}" "${GSA_PRIMARY}"

gcloud services enable \
    anthos.googleapis.com \
    anthosgke.googleapis.com \
    cloudresourcemanager.googleapis.com \
    container.googleapis.com \
    gkeconnect.googleapis.com \
    gkehub.googleapis.com \
    secretmanager.googleapis.com \
    serviceusage.googleapis.com \
    stackdriver.googleapis.com \
    monitoring.googleapis.com \
    logging.googleapis.com

# Connect Agent (currently using primary GSA)
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member="serviceAccount:${GSA_PRIMARY}@${PROJECT_ID}.iam.gserviceaccount.com" \
  --role="roles/gkehub.connect"

# Connect Hub (currently using primary GSA)
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member="serviceAccount:${GSA_PRIMARY}@${PROJECT_ID}.iam.gserviceaccount.com" \
  --role="roles/gkehub.admin"

# Observability agent (currently using primary GSA)
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member="serviceAccount:${GSA_PRIMARY}@${PROJECT_ID}.iam.gserviceaccount.com" \
--role="roles/logging.logWriter"

# Observability agent (currently using primary GSA)
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member="serviceAccount:${GSA_PRIMARY}@${PROJECT_ID}.iam.gserviceaccount.com" \
--role="roles/monitoring.metricWriter"

# Setup default compute service account with secrets accessor access (may not be needed)
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:${PROJECT_NUMBER}-compute@developer.gserviceaccount.com" \
    --role="roles/secretmanager.viewer"

# Create/Setup bucket
setup_bucket "${BUCKET_NAME}" "${PROJECT_ID}"
# Copy configuration
gsutil cp gce-init.sh gs://${BUCKET_NAME}/gce-init.sh

# Create firewall for kubectl based on network tag
gcloud compute firewall-rules create allow-kubectl \
  --action allow \
  --target-tags kubectl \
  --source-ranges 0.0.0.0/0 \
  --rules tcp:6443

# Create secrets for GCE instance to pull down
create_secret "root-repo" "${ROOT_REPO}"

# Create the GCP Secret for gitlab secret
create_secret "${GITLAB_SECRET_USER}" "${GITLAB_SECRET_TOKEN}"

# Create the boot disk
setup_book_disk

# Create the GCE VM (buildling out ABM instances internally)
create_gce_vms

echo ""
echo "---------------"
echo ""
echo "RUN: gcloud beta compute ssh --zone \"us-central1-c\" \"abm-1\" --project \"anthos-bare-metal-lab-1\""
echo ""
echo "RUN: sudo journalctl -fu google-startup-scripts.service"
