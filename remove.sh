#!/bin/bash -x

source ./variables.sh

# Remove firewall
FIREWALL=$(gcloud compute firewall-rules describe allow-kubectl 2> /dev/null)
if [[ $? -eq 0 ]]; then
    gcloud compute firewall-rules delete allow-kubectl --quiet
fi

# Remove VM(s)
remove_all_instances

# Remove cluster configs
gsutil -m rm -r gs://$BUCKET_NAME/kubeconfigs

# remove bmctl
# gsutil -m rm -r gs://$BUCKET_NAME/utils/bmctl

# Remove GCE init script
gsutil rm -r gs://$BUCKET_NAME/gce-init.sh