#!/bin/bash -xe

# TODO: Adjust for sizing exercise
VM_STORAGE="200G"
VM_CPUS="4"
VM_RAM="8192"

GSA_CREDS="/root/abm_creds.json"
SSH_KEY="/root/.ssh/id_rsa"
SSH_KEY_PUB="${SSH_KEY}.pub"
WORKSPACE_DIR="/abm-install"
CLUSTER_NAME=$(curl http://metadata.google.internal/computeMetadata/v1/instance/name -H "Metadata-flavor: Google")
>&2 echo "Starting buildling of ${CLUSTER_NAME}"
echo "STARTING building of ${CLUSTER_NAME}"
CLUSTER="${CLUSTER_NAME}"

## Configure Config Management (RootSync Operator)
SYNC_REPO="$(gcloud secrets versions access latest --secret=root-repo)"
GIT_CRED_SECRET="$(gcloud secrets versions access latest --secret="anthos-demo-acm-user")"

if [[ -z "${GIT_CRED_SECRET}" ]]; then
  # fail fast if secrets are not available
  >&2 echo "Cannot find GIT token secret for ACM from GCP Secret"
  exit 1
fi

if [[ -z "${SYNC_REPO}" ]]; then
  >&2 echo "Cannot find git repo for ACM from GCP Secret"
  exit 1
fi

# Function to replace variables in cluster config
function replace_variables() {
    KEY=$1
    VALUE="$2"

    sed -i "s|${KEY}:.*$|${KEY}: ${VALUE}|g" \
      ${WORKSPACE_DIR}/bmctl-workspace/${CLUSTER_NAME}/${CLUSTER_NAME}.yaml
}

function replace_global() {
    FIND=$1
    REPLACE="$2"

    sed -i "s|${FIND}$|${REPLACE}|g" \
      ${WORKSPACE_DIR}/bmctl-workspace/${CLUSTER_NAME}/${CLUSTER_NAME}.yaml
}

## START

gcloud secrets versions access latest --secret=gsa-creds > ${GSA_CREDS}

PROJECT_ID=$(gcloud config get-value core/project)
REGION=$(gcloud config get-value compute/region 2> /dev/null)
ZONE=$(gcloud config get-value compute/zone 2> /dev/null)

BUCKET_NAME="abm-bootstrapping-${PROJECT_ID}"

if [[ -z "${PROJECT_ID}" ]]; then
    echo "Check variable contents of PROJECT_ID is empty, this is NOT GOOD"
    exit 1
fi

# Insert pub key for easy gcloud access
gcloud compute config-ssh

# Install KVM
apt-get update && apt-get install qemu-kvm -y

# Install kubectl
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"

chmod +x kubectl

mv kubectl /usr/local/bin/

# Install bmctl
gsutil cp gs://${BUCKET_NAME}/utils/bmctl /usr/local/bin/bmctl
chmod 777 /usr/local/bin/bmctl

# Install docker
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh

# Setup bridge network for node/worker VMs

apt update && apt install uml-utilities qemu-kvm \
  bridge-utils virtinst libvirt-daemon-system libvirt-clients -y

################################

LIBVIRT_HOME="/var/lib/libvirt/images"
IMAGE_BASE_DIR="${LIBVIRT_HOME}/base"

# Setup IP addresses for virtual network
virsh net-dumpxml default > net-default.xml
sed -i 's/192.168.122.1/10.200.0.1/g' net-default.xml
sed -i "s/192.168.122.2'/10.200.0.100'/g" net-default.xml
sed -i "s/192.168.122.254'/10.200.0.254'/g" net-default.xml
virsh net-destroy default
virsh net-define net-default.xml
virsh net-start default
virsh net-autostart default

sleep 30s

mkdir -p ${IMAGE_BASE_DIR}
# TODO: Should this come from a GCS bucket instead (for future)?
wget -O ${IMAGE_BASE_DIR}/ubuntu-20.04.qcow2 https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img

mkdir ${LIBVIRT_HOME}/{vm1,vm2}

qemu-img create -f qcow2 -F qcow2 \
  -o backing_file=${IMAGE_BASE_DIR}/ubuntu-20.04.qcow2 \
  ${LIBVIRT_HOME}/vm1/disk.qcow2

qemu-img create -f qcow2 -F qcow2 \
  -o backing_file=${IMAGE_BASE_DIR}/ubuntu-20.04.qcow2 \
  ${LIBVIRT_HOME}/vm2/disk.qcow2

qemu-img resize ${LIBVIRT_HOME}/vm1/disk.qcow2 ${VM_STORAGE}
qemu-img resize ${LIBVIRT_HOME}/vm2/disk.qcow2 ${VM_STORAGE}

# Get JSON Key for GSA (ignore failures, redirect success into file)
gcloud secrets versions access latest --secret=gsa-creds > ${GSA_CREDS}
SECRET_SIZE=$(stat -c%s "${GSA_CREDS}")
if [[ "${SECRET_SIZE:-0}" -eq "0" ]]; then
  >&2 echo "ERROR: GSA Secret was not found and/or not stored in proper location"
  exit 1
fi

# Creates a key for the root user (used later in script also)
ssh-keygen -t rsa -b 4096 -C "root@local" -f "${SSH_KEY}" -q -N ""

export PUB_KEY=$(cat "${SSH_KEY_PUB}")

cat >user-data <<EOF
#cloud-config
users:
  - name: root
    ssh-authorized-keys:
      - $PUB_KEY
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    groups: sudo
    shell: /bin/bash
disable_root: false
runcmd:
  - echo "AllowUsers root" >> /etc/ssh/sshd_config
  - echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
  - systemctl restart ssh
  - systemctl stop apparmor.service
  - systemctl disable apparmor.service
  - ip link set mtu 1460 dev enp1s0
EOF

cat >meta-data <<EOF
local-hostname: vm1
instance-id: vm1
EOF
genisoimage  -output ${LIBVIRT_HOME}/vm1/cidata.iso -volid cidata -joliet -rock user-data meta-data

cat >meta-data <<EOF
local-hostname: vm2
instance-id: vm2
EOF

genisoimage  -output ${LIBVIRT_HOME}/vm2/cidata.iso -volid cidata -joliet -rock user-data meta-data

virt-install --connect qemu:///system --virt-type kvm --name vm1 \
  --ram ${VM_RAM} --vcpus ${VM_CPUS} --os-type linux --os-variant ubuntu20.04 \
  --disk path=${LIBVIRT_HOME}/vm1/disk.qcow2,format=qcow2 \
  --disk ${LIBVIRT_HOME}/vm1/cidata.iso,device=cdrom \
  --import --network network=default --noautoconsole

virt-install --connect qemu:///system --virt-type kvm --name vm2 \
  --ram ${VM_RAM} --vcpus=${VM_CPUS} --os-type linux --os-variant ubuntu20.04 \
  --disk path=${LIBVIRT_HOME}/vm2/disk.qcow2,format=qcow2 \
  --disk ${LIBVIRT_HOME}/vm2/cidata.iso,device=cdrom \
  --import --network network=default --noautoconsole

sleep 60 # Wait for VMs to boot up
# TODO: Create a loop on the IP for up to X minutes or iterations
# wait a minute or so to ensure all VMs have boot up
export VM1_IP=$(virsh domifaddr vm1 | grep vnet \
  | awk '{ print $4 }' | sed 's/...$//')

export VM2_IP=$(virsh domifaddr vm2 | grep vnet \
  | awk '{ print $4 }' | sed 's/...$//')

if [[ -z "${VM1_IP}" ]]; then
  >&2 echo "IP for VM1 was NOT established"
fi

if [[ -z "${VM2_IP}" ]]; then
  >&2 echo "IP for VM2 was NOT established"
fi

#################################################################
#################################################################
##### Install Cluster
#################################################################
#################################################################

mkdir -p ${WORKSPACE_DIR}
cd ${WORKSPACE_DIR}

# Create cluster configuration
bmctl create config -c ${CLUSTER_NAME} --project-id ${PROJECT_ID}

replace_variables "gcrKeyPath" "${GSA_CREDS}"
replace_variables "sshPrivateKeyPath" "${SSH_KEY}"
replace_variables "gkeConnectAgentServiceAccountKeyPath" "${GSA_CREDS}"
replace_variables "gkeConnectRegisterServiceAccountKeyPath" "${GSA_CREDS}"
replace_variables "cloudOperationsServiceAccountKeyPath" "${GSA_CREDS}"

replace_variables "type" "standalone"

replace_variables "projectID" "${PROJECT_ID}"

replace_global "address: <Machine 1 IP>" "address: ${VM1_IP}"
replace_global "address: <Machine 2 IP>" "address: ${VM2_IP}"
replace_global "- address: <Machine 3 IP>" "address: ${VM1_IP}"

# POD IP ranges
replace_global "- 192.168.0.0/16" "- 172.168.0.0/16"

replace_global "# ingressVIP: 10.0.0.2" "ingressVIP: 10.200.0.10"

replace_global "# addressPools:" "addressPools:"
replace_global "# - name: pool1" "- name: pool1"
replace_global "#   addresses:" "  addresses:"
replace_global "#   - 10.0.0.1-10.0.0.4" "  - 10.200.0.10-10.200.0.30"

replace_variables "controlPlaneVIP" "10.200.0.9"


echo "*****************************"
cat ${WORKSPACE_DIR}/bmctl-workspace/${CLUSTER_NAME}/${CLUSTER_NAME}.yaml
echo "*****************************"


bmctl check preflight -c ${CLUSTER_NAME}

if [[ $? > 0 ]]; then
  >&2 echo "Error with the pre-flight check"
  exit 1
fi

bmctl create cluster -c ${CLUSTER_NAME}

sleep 60s # 1 minute

# Copy the generated kubeconfig to workspace folder
cp ${WORKSPACE_DIR}/bmctl-workspace/${CLUSTER_NAME}/${CLUSTER_NAME}-kubeconfig ${WORKSPACE_DIR}/kubeconfig
## Setup environment for multiple users (ie, anyone can read the kubeconfig if you're SSH'd into the box)
chmod -R o+r ${WORKSPACE_DIR}

# Setup kubeconfig
export KUBECONFIG="${WORKSPACE_DIR}/kubeconfig"

echo -e "\nexport KUBECONFIG='${KUBECONFIG}'" >> /etc/profile
echo -e "\nsource <(kubectl completion bash)\n" >> /etc/profile

kubectl cluster-info

gsutil cp ${WORKSPACE_DIR}/bmctl-workspace/${CLUSTER_NAME}/${CLUSTER_NAME}-kubeconfig gs://${BUCKET_NAME}/kubeconfig/${CLUSTER_NAME}/kubeconfig.json
gsutil cp ${WORKSPACE_DIR}/bmctl-workspace/${CLUSTER_NAME}/${CLUSTER_NAME}.yaml gs://${BUCKET_NAME}/kubeconfig/${CLUSTER_NAME}/${CLUSTER_NAME}.yaml # Debugging purposes

####################################################
####################################################
##### Install ACM Multi-Repo
####################################################
####################################################

# Install ACM (Config Management base operator)
gsutil cp gs://config-management-release/released/latest/config-management-operator.yaml config-management-operator.yaml
kubectl apply -f config-management-operator.yaml

sleep 10s # needs a few seconds for the API to accept the operator, the "wait" command fails immediately without it...technically, only 1s would be required, but being safe with 10s

# Wait for ConfigManagement & RootSync CRDs
kubectl wait --for=condition=established --timeout=60s crd configmanagements.configmanagement.gke.io

## Setup Config Management (Operator/Controller)

cat > config-management.yaml <<EOF
apiVersion: configmanagement.gke.io/v1
kind: ConfigManagement
metadata:
  name: config-management
spec:
  # clusterName is required and must be unique among all managed clusters
  clusterName: ${CLUSTER_NAME}
  enableMultiRepo: true
EOF

kubectl apply -f config-management.yaml
sleep 10s # need a brief moment for API to accept configmanagement
kubectl wait --for=condition=established --timeout=120s crd rootsyncs.configsync.gke.io

# Create git-creds secret to view root sync repo
kubectl create secret generic git-creds \
  --namespace="config-management-system" \
  --from-literal="username=anthos-demo-acm-user" \
  --from-literal=token="${GIT_CRED_SECRET}"

# Probably not needed, but better safe (there is no "wait" command for secrets)
sleep 5s

cat > root-sync-config.yaml <<EOF
apiVersion: configsync.gke.io/v1alpha1
kind: RootSync
metadata:
  name: root-sync
  namespace: config-management-system
spec:
  sourceFormat: hierarchy
  git:
    repo: "${SYNC_REPO}"
    branch: "main" # NOTE
    dir: "config"
    auth: "token"
    secretRef:
      name: "git-creds"
EOF

cat root-sync-config.yaml

kubectl apply -f root-sync-config.yaml

sleep 5s # short pause before the root reconsiler will be in the API to be queried against

# Wait for the "root-reconsiler" deployment to become available
kubectl wait --for=condition=available --timeout=600s \
  deployment root-reconciler \
  -n config-management-system

echo "Done!!!"
