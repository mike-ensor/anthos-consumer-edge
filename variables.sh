#!/bin/bash

###### GENERAL VARIABLES -- unlikely needing to change ######
export PROJECT_ID=$(gcloud config list --format 'value(core.project)')
export PROJECT_NUMBER=$(gcloud projects describe ${PROJECT_ID} --format 'value(projectNumber)')
export REGION=$(gcloud config get-value compute/region 2> /dev/null)
export ZONE=$(gcloud config get-value compute/zone 2> /dev/null)
export ACCOUNT_EMAIL=$(gcloud auth list --filter=status:ACTIVE --format="value(account)")
export USERNAME=$(echo "${ACCOUNT_EMAIL}" | cut -d@ -f1)
export BUCKET_NAME="abm-bootstrapping-${PROJECT_ID}" # CAREFUL changing this, the "gce-init.sh" process uses this name by convention only

export ABM_VERSION="0.7.0-gke.2"
export GCE_STORAGE_SIZE="600G" # needs 200G x 2 for VMs ( #TODO Configure for smaller footprint? )

# GSAs
export ABM_GSA="baremetal-gcr" # MUST already exist due to allowlisting ahead of time

# Primary GSA
export GSA_PRIMARY="${ABM_GSA}"
export GSA_PRIMARY_SECRET_KEY="${GSA_PRIMARY}-key"
# Connect Agent
export GSA_CONNECT_AGENT="gsa-connect-agent"
export GSA_CONNECT_AGENT_SECRET_KEY="${GSA_CONNECT_AGENT}-key"
# GKE Hub
export GSA_GKE_HUB="gsa-gke-hub"
export GSA_GKE_HUB_SECRET_KEY="${GSA_GKE_HUB}-key"
# Observability
export GSA_OBSERVABILITY="gsa-observability"
export GSA_OBSERVABILITY_SECRET_KEY="${GSA_OBSERVABILITY}-key"
########################################################################
########################################################################
###### User specific variables (defaults are in repo) ##################
########################################################################
########################################################################
export MACHINE_SIZE="n1-standard-16" ## "n2-standard-16" n2-standard-8 -- # n2-custom (6 vCPUs, 21.25 GB memory) recommended
# Host does not support KVM virtualization for "e2-standard-16"
# --machine-type=n2-8-16 --machine-type=n2-custom-number-of-cpus-number-of-mb
export GITLAB_SECRET_TOKEN="sXEkys1KwRq_n1sh46aV" # READ ONLY token (should be pretected in future)
export GITLAB_SECRET_USER="anthos-demo-acm-user"
export ROOT_REPO="https://gitlab.com/mike-ensor/acm-root-repository.git"

# If starting index was specified at script runtime, use that number
if [[ ! -z "${STARTING_INDEX}" ]]; then
    export CLUSTER_START_INDEX=${STARTING_INDEX}
else
    export CLUSTER_START_INDEX=1
fi

########################################################################
########################################################################

function bmctl_installed() {
    location=$(which bmctl)
    if [[ -z "${location}" ]]; then
        echo "bmctl binary is required for installation, please install and place somewhere in the PATH"
        exit 1
    fi
}

# Create a secret and version; if secret already exists, adds a version. Idempotent(ish)
function create_secret() {
    KEY="$1"
    VALUE="$2"
    FILE="${3-false}"
    gcloud secrets -q versions access latest --secret="${KEY}" 2> /dev/null
    if [[ $? > 0 ]]; then
        gcloud secrets create ${KEY} --replication-policy="automatic"
    fi
    if [[ "$FILE" == "false" ]]; then
        # Standard Input
        echo ${VALUE} | gcloud secrets versions add ${KEY} --data-file=-
    else
        # File reference
        gcloud secrets versions add ${KEY} --data-file=${VALUE}
    fi
}

# Removes a secret
function delete_secret() {
    KEY="$1"
    gcloud secrets delete ${KEY} --quiet
}

# Create storage bucket for Init Script
function setup_bucket() {
    local BUCKET=${1-$BUCKET_NAME}
    local PROJECT=${2-$PROJECT_ID}
    gsutil -q stat gs://${BUCKET}/.dontremove 2> /dev/null
    if [[ $? > 0 ]]; then
        echo "Bucket does not exist, creating gs://${BUCKET}"
        gsutil mb -p ${PROJECT} gs://${BUCKET}
        if [[ $? > 0 ]]; then
            echo "Error: Cannot create bucket ${BUCKET} in ${PROJECT}"
            exit 1
        fi
        # Add file for flag that bucket is available
        echo "do not remove this file" | gsutil cp - gs://$BUCKET_NAME/.dontremove
    fi
}

function create_enable_service_account() {
    local SA_NAME="$1"
    local PROJECT="${2-$PROJECT_ID}"
    local DESCRIPTION="${3-Service account for GKE Hub}"
    local CURR_SA_NAME=$(gcloud iam service-accounts describe ${SA_NAME}@${PROJECT}.iam.gserviceaccount.com --format="value(name)" 2> /dev/null)
    if [ -z "$CURR_SA_NAME" ]; then
        gcloud iam service-accounts create ${SA_NAME} --display-name "${DESCRIPTION}"
    else
        gcloud iam service-accounts enable "${SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com"
    fi
}

function disable_service_account() {
    local SA_NAME="$1"
    local PROJECT="${2-$PROJECT_ID}"
    local CURR_SA_NAME=$(gcloud iam service-accounts describe ${SA_NAME}@${PROJECT}.iam.gserviceaccount.com --format="value(name)" 2> /dev/null)
    if [ ! -z "$CURR_SA_NAME" ]; then
        gcloud iam service-accounts disable "${SA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" -q
    fi
}

function remove_instance_in_zone() {
    INSTANCES=$1
    INSTANCE_ZONE=$2

    ARRAY=(${INSTANCES})
    for INSTANCE in "${ARRAY[@]}"; do
        gcloud container hub memberships delete ${INSTANCE} --quiet --async 2> /dev/null
    done

    gcloud compute instances delete ${INSTANCES} --zone ${INSTANCE_ZONE} --delete-disks=all --quiet
}

function remove_all_instances() {
    INSTANCE_ZONE="${ZONE}" # TODO Hardcoded zone (ie, not multi-zone yet)
    i=0
    for i in $(seq 1 $zone_count); do # 1-based seqence
        INSTANCES=$(gcloud compute instances list --zones ${INSTANCE_ZONE} --filter="labels.type:edge-server" --format="value(name)" | xargs | sed -e 's/ / /g')
        # Delete Instances in the zone
        if [[ ! -z "${INSTANCES}" ]]; then
            echo "Removing $INSTANCES in ${INSTANCE_ZONE}"
            remove_instance_in_zone "${INSTANCES}" "${INSTANCE_ZONE}"
        fi
    done
}

