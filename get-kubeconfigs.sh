#!/bin/bash

source ./variables.sh

CONFIG_FOLDER="kubeconfigs"

if [[ -d ./kubeconfigs ]]; then
    rm -rf ./kubeconfigs
fi

# Downloading all configs
echo "Remote kubectonfigs do not exist, downloading to local machine..."
gsutil -m cp -r gs://${BUCKET_NAME}/${CONFIG_FOLDER} .

for dir in ./${CONFIG_FOLDER}/*; do
    DIR="$dir"
    CLUSTER="${DIR##./*/}"
    echo "Setting up KUBECONFIG for '${CLUSTER}'"
    IP=$(gcloud compute instances list --filter="name=$CLUSTER" --format="value(networkInterfaces.accessConfigs.natIP)")
    IP2="${IP//[\'/}"
    FINAL_IP="${IP2//\']/}"

    echo "Final IP: ${FINAL}"
    sed -i "s/127.0.0.1/$FINAL_IP/g" ${dir}/cluster1.yaml

    cat ${dir}/cluster1.yaml

    echo "export KUBECONFIG=./cluster1.yaml" > $dir/.envrc
done