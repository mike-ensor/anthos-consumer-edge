# Overview

This project builds out GCE instances with Anthos BareMetal in a minimal sizing intended to demonstrate a Consumer Edge architecture

## Hub-Spoke

Each GCE VM will represent a physical location that is managed by a centralized Anthos Config Management repository structure.

### Locations

Each VM/location will represent one of the following:

* Store/Retail, Manufacturing line/plant, or Warehouse
* Isolated or "island" instances (ie, store-to-store communication is treated going over public network)
* Homogenous installations with a S/M/L footprint configuration based on local compute need

# Running

## Setup

The `setup.sh` file is responsible for kicking off the automation of GCE instances. Each GCE instance will pull their `init-script` from a GCS bucket created/managed in the `setup.sh` script and will build asynchronous from each other.  Each run will take in an integer for the number of instances desired.

```bash
./setup.sh -c <INT>
```

## Remove

The `remove.sh` file will remove all GCE instances labeled with `type=abm-vm`

### Upcoming changes

* Adding region support to build into (for automation distribution purposes, not "regional" compute)